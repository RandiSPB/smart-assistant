# Start project

## Docker start

### Docker build

1. Create an docker image
```
docker build .
```

If you wanna identify image by name

```
 docker build -t {repository_name} .
```

2. Start the container
```
docker run {image_id or image_tag}
```

### Docker compose

```
docker-compose up
```

*P.S. This case working only on theory, I have troubles with shared library and I`ll be glad to get help to fix this*


## Python native start
1. Install requirements
```
pip install -r /requirements/requirements.txt
```

2. Run service

On UNIX`s systems:
```
python3 app/main.py
```
On windows:
```
python .\app\main.py
```