from typing import Union

from pydantic import AnyHttpUrl, AnyUrl, BaseSettings, validator
from pydantic.tools import parse_obj_as


class Settings(BaseSettings):
    ENV_STATE: str = "Development "
    DEFAULT_HOST: str = "localhost"
    DEFAULT_PORT: int = 8001
    DEBUG: bool = True
    SERVICE_NAME: str = "Voice recognition service"
    API_PREFIX: str = ""
    BACKEND_CORS_ORIGINS: list[AnyHttpUrl] = []

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, list[str]]) -> Union[list[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        if isinstance(v, (list, str)):
            return v

        raise ValueError(v)

    class Config:
        env_file = ".env"
        case_sensitive = True


settings = Settings()
