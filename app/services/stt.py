import json
import wave
from fastapi import UploadFile
from typing import Union

from vosk import KaldiRecognizer, Model


class SpeechToTextService:
    def __init__(self) -> None:
        self.recognition_engine = KaldiRecognizer(Model("model"), 16000)
        self.recognition_engine.SetWords(True)

    async def voice_recognition(
        self, audiofile: UploadFile
    ) -> dict[str, Union[list[str], str]]:
        audio = wave.open(audiofile.file, "rb")

        while True:
            data = audio.readframes(4000)
            if len(data) == 0:
                break
            if self.recognition_engine.AcceptWaveform(data):
                print(json.loads(self.recognition_engine.Result()))
            else:
                print(json.loads(self.recognition_engine.PartialResult()))

        return json.loads(self.recognition_engine.FinalResult())
