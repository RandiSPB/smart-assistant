from app.services.stt import SpeechToTextService
from app.services.tts import TextToSpeechService

tts_service = TextToSpeechService()
stt_service = SpeechToTextService()
