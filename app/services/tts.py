import os

import pyttsx3


class TextToSpeechService:
    def __init__(self, language: str = "ru"):
        self.voice_engine = pyttsx3.init()
        self.voice_engine.setProperty(
            "voice", self.voice_engine.getProperty("voices")[2].id
        )

    async def create_voice_file(self, text: str, client: str) -> bytes:
        filename: str = f"audio_{client}.wav"
        self.voice_engine.save_to_file(text, filename)
        self.voice_engine.runAndWait()
        assistant_message: bytes = self.read_audio_file(filename)
        os.remove(filename)
        return assistant_message

    @staticmethod
    def read_audio_file(filename: str) -> bytes:
        with open(filename, "rb") as voice_message:
            return voice_message.read()
