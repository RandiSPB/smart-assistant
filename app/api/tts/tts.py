from io import BytesIO

from fastapi import APIRouter, Request
from starlette.responses import StreamingResponse

from app.services import tts_service

router = APIRouter()


@router.get("/get_voice_command", status_code=200)
async def get_voice_command(text: str, request: Request) -> StreamingResponse:
    client_data = request.client.host
    assistant_message = await tts_service.create_voice_file(text, client_data)
    return StreamingResponse(BytesIO(assistant_message), media_type="audio/x-wav")
