from fastapi import APIRouter

from app.api.tts import tts

router = APIRouter(tags=["text - to - speech"])
router.include_router(tts.router)
