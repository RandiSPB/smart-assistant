from fastapi import APIRouter

from app.api import stt, tts

router = APIRouter()
router.include_router(tts.router)
router.include_router(stt.router)
