from fastapi import APIRouter, Request, UploadFile
from typing import Union
from app.services import stt_service

router = APIRouter()


@router.post("/send_voice", status_code=200)
async def send_voice_message(payload: UploadFile, request: Request) -> dict[str, Union[list[str], str]]:
    return await stt_service.voice_recognition(payload)
