from fastapi import APIRouter

from app.api.stt import stt

router = APIRouter(tags=["speech - to - text"])
router.include_router(stt.router)
