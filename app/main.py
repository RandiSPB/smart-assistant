from wave import Error

import uvicorn
from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from starlette.middleware.cors import CORSMiddleware

from app.api import router
from app.core.config import settings

app = FastAPI(title="Assistant voice service")

if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


@app.exception_handler(ValueError)
async def audio_exception_handler(request: Request, exc: ValueError) -> JSONResponse:
    return JSONResponse(status_code=500, content={"message": exc})


@app.exception_handler(Error)
async def audio_type_exception_handler(request: Request, exc: Error) -> JSONResponse:
    return JSONResponse(
        status_code=500, content={"message": "File should be WAV format"}
    )


app.include_router(router)


if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host=settings.DEFAULT_HOST,
        port=settings.DEFAULT_PORT,
        reload=settings.DEBUG,
    )
