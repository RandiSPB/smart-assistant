FROM python:3.9.7-slim

ENV PROJECT_NAME=/smart-assistant
ENV MODEL_NAME=vosk-model-small-ru-0.22
ENV MODEL_LINK=https://alphacephei.com/vosk/models/$MODEL_NAME.zip
ENV MODEL_ARCH_NAME=model.zip
ENV NORMAL_MODEL_PATH=app/model

WORKDIR ${PROJECT_NAME}

COPY ./requirements/requirements.txt /${PROJECT_NAME}/requirements.txt

RUN apt update && apt install -y espeak curl unzip
RUN pip install --no-cache-dir --upgrade -r ${PROJECT_NAME}/requirements.txt

COPY ./app ${PROJECT_NAME}/app

RUN rm -r $NORMAL_MODEL_PATH
RUN curl $MODEL_LINK --output $MODEL_ARCH_NAME
RUN unzip $MODEL_ARCH_NAME
RUN mv $MODEL_NAME model
RUN rm $MODEL_ARCH_NAME

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]